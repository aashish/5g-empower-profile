#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN


tourDescription = """
# 5G-Empower Controlled RF
Use this profile to intantiate the 5G-Empower software defined networking platform
within a controlled RF environment and end-to-end LTE network
(wired connections between UE and eNB). The UE can be srsLTE-based or a Nexus 5.

If you elect to use a Nexus 5, these nodes will be deployed:
* Nexus 5 (`rue1`)
* Generic Compute Node w/ ADB image (`adbnode`)
* Generic Compute Node w/ Empower image (`controller`)
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)

If instead you choose to use an srsLTE UE, these will be deployed:
* Intel NUC5300/B210 w/ srsLTE (`rue1`) or
* Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)
"""
tourInstructions = """
NOTE: Ensure that the startup has finished for both eNB and controller via the powder ssh list view.

### 5G-Empower Controller
Login to `controller` and do:
```
# start empower controller
cd /opt/empower-runtime/ && sudo ./empower-runtime.py
```

For information about configuring the controller visit:

https://github.com/5g-empower/5g-empower.github.io/wiki/Configuring-up-the-Controller

### Start EPC and eNB
After your experiment becomes ready, login to `enb1` via `ssh` and run the commands below in separate tabs:
```
# start srsepc
sudo srsepc /etc/srslte/epc.conf
# start srsenb
cd /opt/srsLTE-20.04/build/srsenb/src && sudo ./srsenb enb.conf
```
This will start the srsEPC and srseNB respectively. After you've associated
a UE with this eNB, you can use the third tab to run tests with `ping` or
`iperf`.
NOTE: If the radio is not detected, power cycle the eNB node.
### Nexus 5
If you've deployed a Nexus 5, you should see it sync with the eNB eventually and
obtain an IP address. Login to `adbnode` in order to access `rue1` via `adb`:
```
# on `adbnode`
pnadb -a
adb shell
```
Once you have an `adb` shell to `rue1`, you can use `ping` to test the
connection, e.g.,
```
# in adb shell connected to rue1
# ping SGi IP
ping 172.16.0.1
```
If the Nexus 5 fails to sync with the eNB, try rebooting it via the `adb` shell.
After reboot, you'll have to repeat the `pnadb -a` and `adb shell` commands on
`adbnode` to reestablish a connection to the Nexus 5.
### srsLTE UE
If you've deployed an srsLTE UE, login to `rue1` and do:
```
/local/repository/bin/start.sh
```
This will start a `tmux` session with two panes: one running srsue and the other
holding a spare terminal for running tests with `ping` and `iperf`.
If you are not familiar with `tmux`, it's a terminal multiplexer that
has some similarities to screen. Here's a [tmux cheat
sheet](https://tmuxcheatsheet.com), but `ctrl-b o` (move to other pane) and
`ctrl-b x` (kill pane), should get you pretty far. `ctrl-b d` will detach you
from the `tmux` session and leave it running in the background. You can reattach
with `tmux attach`. If you'd like to run `srsue` manually, do:
```
sudo srsue /etc/srslte/ue.conf
```

"""
#Experimental Images
#Controller image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-Controller
#nextEPC eNb image: urn:publicid:IDN+emulab.net+image+reu2020:Empower-eNb-epc
#no next eNB image: urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1


class GLOBALS(object):
    NUC_HWTYPE = "nuc5300"
    COTS_UE_HWTYPE = "nexus5"
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    CONTROL_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops:UBUNTU20-64-STD" #Used Ubuntu20 for better python support
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderProfiles:U18LL-SRSLTE:1" #Low-latency image
    ENB_IMG = "urn:publicid:IDN+emulab.net+image+reu2020:srsLTE_Empower.enb1"
    COTS_UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")


pc = portal.Context()
#Allow the user to use a Nexus 5 or srsUE
pc.defineParameter("ue_type", "UE Type", portal.ParameterType.STRING, "nexus5",
                   [("srsue", "srsLTE UE (B210)"), ("nexus5", "COTS UE (Nexus 5)")],
                   longDescription="Type of UE to deploy.")
#Allow the user to specify a specific UE
pc.defineParameter("FIXED_UE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF UE node to allocate (e.g., 'ue1').  Leave blank to let the mapping algorithm choose.")
#Allow the user to specify a specific eNb
pc.defineParameter("FIXED_ENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF eNodeB device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")


params = pc.bindParameters()

#Check for exceptions and warnings
pc.verifyParameters()

#Create model of the RSpec
request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb1 = request.RawPC("enb1")
if params.FIXED_ENB:
        enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
# enb1.disk_image = GLOBALS.SRSLTE_IMG
enb1.disk_image = GLOBALS.ENB_IMG
enb1.Desire("rf-controlled", 1)
enb1_rue1_rf = enb1.addInterface("rue1_rf")
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
# enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/install_eNB.sh"))

#Add a Controller Node
controller = request.RawPC("controller")
controller.disk_image = GLOBALS.CONTROL_IMG
controller.addService(rspec.Execute(shell="bash", command="/local/repository/bin/Controller-Script.sh"))

#Add an EPC Node
#epc = request.RawPC("epc")
#epc.disk_image = GLOBALS.SRSLTE_IMG
#epc.addService(rspec.Execute(shell="bash", command="/local/repository/bin/install_nextEPC.sh"))

# Add a UE node
if params.ue_type == "nexus5":
    adbnode = request.RawPC("adbnode")
    adbnode.disk_image = GLOBALS.ADB_IMG
    rue1 = request.UE("rue1")
    if params.FIXED_UE: #Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.COTS_UE_HWTYPE
    rue1.disk_image = GLOBALS.COTS_UE_IMG
    rue1.adb_target = "adbnode"
elif params.ue_type == "srsue":
    rue1 = request.RawPC("rue1")
    if params.FIXED_UE: #Use a specific UE
        rue1.component_id = params.FIXED_UE
    rue1.hardware_type = GLOBALS.NUC_HWTYPE
    rue1.disk_image = GLOBALS.SRSLTE_IMG
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    rue1.addService(rspec.Execute(shell="bash", command="/local/repository/tune-cpu.sh"))

rue1.Desire("rf-controlled", 1)
rue1_enb1_rf = rue1.addInterface("enb1_rf")

# Create the RF link between the UE and eNodeB
rflink = request.RFLink("rflink")
rflink.addInterface(enb1_rue1_rf)
rflink.addInterface(rue1_enb1_rf)

# Create the LAN and attach the devices
epclink = request.Link("s1-lan")
#epclink.addNode(epc)
epclink.addNode(enb1)
epclink.addNode(controller)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
